Mongo Commander
===============

A small library implementing the commander pattern to execute mongo queries

## Installation

  npm install mongocommander --save

## Usage

  var mongoCommander = require('scapegoat');

  mongoCommander.pCreateHelper('User', {
    email: 'martin@interaction.cr'
  });

## Tests

  npm test

## Contributing

In lieu of a formal styleguide, take care to maintain the existing coding style.
Add unit tests for any new or changed functionality. Lint and test your code.

## Release History

* 0.0.1 Initial release (Using mongo native through mongoose)
* 0.0.2 Mongoose removed, wrapping native mongodb
* 0.0.3 Added Ensure index function, fixed create helper default options bug