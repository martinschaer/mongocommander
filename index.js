/* global module */
'use strict';

// -----------------------
// Deps
// -----------------------
var MongoClient = require('mongodb').MongoClient,
  ObjectId = require('mongodb').ObjectID,
  when = require('when'),
  //ABRIR VARIAS CONEXIONES ES BAJO EN PERFORMANCE 
  //EL REQUEST TCP PUEDE TARDAR 100s de milisegundos
  dbInstance = null,
  dbOpened = false,
  connectionUrl = null,
  idParser = function idParser(query) {
    //TODO REVISAR MAS OPERACIONES DONDE HAYA QUE HACER EL PARSE
    var i = 0;
    if (query._id.$in) {
      for (i = 0; i < query._id.$in.length; i++) {
        query._id.$in[i] = new ObjectId(query._id.$in[i]);
      }
      return query;
    }
    if (query._id.$ne) {
      query._id.$ne = new ObjectId(query._id.$ne);
      return query;
    }
    if (query._id.$nin) {
      for (i = 0; i < query._id.$in.length; i++) {
        query._id.$nin[i] = new ObjectId(query._id.$nin[i]);
      }
      return query;
    }
    query._id = new ObjectId(query._id);
    return query;
  },
  _pOpenConn = function _pOpenConn() {
    return when.promise(function(resolve, reject) {
      try {
        if (!connectionUrl) {
          return reject(new Error('The server url has not been set up'));
        }
        if (!dbInstance) {
          MongoClient.connect(connectionUrl, function(err, db) {
            if (err) {
              return reject(err);
            }
            dbInstance = db;
            dbOpened = true;
            return resolve(dbInstance);
          });
        } else {
          if (!dbOpened) {
            dbInstance.open(function(err, client) {
              return resolve(dbInstance);
            });
          } else {
            return resolve(dbInstance);
          }
        }
      } catch (err) {
        return reject(err);
      }
    });
  },
  _pCloseConn = function _pCloseConn() {
    return when.promise(function(resolve, reject) {
      if (!dbInstance) {
        return reject(new Error('The conection is not opened'));
      } else {
        dbOpened = false;
        dbInstance.close();
        return resolve();
      }
    });
  };;


/**
 * Abre la conexión con la base de datos
 * returns: <Promise>
 */
module.exports.pOpenConn = function pOpenConn() {
  return _pOpenConn();
};

/**
 * Cierra la conexión con la base de datos
 * returns: <Promise>
 */
module.exports.pCloseConn = function pCloseConn() {
  return _pCloseConn();
};

/**
 * Crea indices con sus respectivas opciones
 * - collectionName <String> Nombre del collection
 * - keys <Object> indices para registrar con sus tipos
 * - options <Object> opciones para los indices a registrar
 * returns: <Promise>
 */
module.exports.pEnsureIndex =
  function pEnsureIndex(collectionName, keys, options) {
    return when.promise(function(resolve, reject) {
      _pOpenConn()
        .then(function(db) {
          db
            .collection(collectionName)
            .ensureIndex(keys, options || {}, function(err, result) {
              if (err) {
                return reject(err);
              } else {
                return resolve(result);
              }

            });
        }, function(err) {
          return reject(err);
        });
    });
  }

/**
 * Guarda el connection string que se va a usar para los queries
 * hace un conexion de prueba para verificar que el string es válido
 * - connectUrl <String> connection string (url de la db)
 * returns: <Promise>
 */
module.exports.pSetUpUrl = function pSetUpUrl(connectUrl) {
  return when.promise(function(resolve, reject) {
    try {
      connectionUrl = connectUrl;
      _pOpenConn()
        .then(function() {
          return resolve('Connected correctly to db server');
        }, function(err) {
          return reject(err);
        });
    } catch (err) {
      return reject(err);
    }
  });
};

/**
 * Actualiza el modelo dado con FIND AND MODIFY
 * - collectionName <String> Nombre del collection
 * - query <Object> criterio de busqueda del registro
 * - sort <Object> criterio de orden de los resultados
 * - update <Object> criteria de actualizacion
 * - options <Object> opciones de actualizacion
 * returns: <Promise>
 */
module.exports.pFindAndModHelper =
  function pFindAndModHelper(collectionName, query, sort, update, options) {
    var promise;
    //
    query = typeof query === 'undefined' ? {} : query;
    //
    if (query._id) {
      query = idParser(query);
    }
    //
    promise = when.promise(function(resolve, reject) {
      try {
        _pOpenConn()
          .then(function(db) {
              var collection = db.collection(collectionName);
              collection.findAndModify(
                query,
                sort,
                update,
                options || {},
                function(err, result) {
                  if (err) {
                    return reject(err);
                  } else {
                    return resolve(result.value);
                  }
                });
            },
            function(err) {
              return reject(err);
            });
      } catch (err) {
        reject(err);
      }
    });
    return promise;
  };

/**
 * Actualiza el modelo dado
 * - collectionName <String> Nombre del collection
 * - query <Object> criterio de busqueda del registro
 * - update <Object> criteria de actualizacion
 * - options <Object> opciones de actualizacion
 * returns: <Promise>
 */
module.exports.pUpdateHelper =
  function pUpdateHelper(collectionName, query, update, options) {
    var promise;
    //
    query = typeof query === 'undefined' ? {} : query;
    //
    if (query._id) {
      query = idParser(query);
    }
    //
    promise = when.promise(function(resolve, reject) {
      try {
        _pOpenConn()
          .then(function(db) {
              var collection = db.collection(collectionName);
              collection.update(
                query,
                update,
                options || {},
                function(err, result) {
                  if (err) {
                    return reject(err);
                  } else {
                    return resolve(result.toJSON());
                  }
                });
            },
            function(err) {
              return reject(err);
            });
      } catch (err) {
        reject(err);
      }
    });
    return promise;
  };

/**
 * Inserta un registro al modelo
 * - collectionName <String> Nombre del collection
 * - struct <Object> struct del modelo o array de structs
 * - optExtention <Object> opciones extras de insercion
 * returns: <Promise> //Resultados son un array
 */
module.exports.pCreateHelper =
  function pCreateHelper(collectionName, struct, options) {
    var promise,
      defaultOptions = {
        w: 1
      };
    if (!options ||
      Object.prototype.toString.call(options) !== '[object Object]') {
      options = defaultOptions;
    }
    for (var x in defaultOptions) {
      if (typeof options[x] === 'undefined') {
        options[x] = defaultOptions[x];
      }
    }
    promise = when.promise(function(resolve, reject) {
      try {
        _pOpenConn()
          .then(function(db) {
            var collection = db.collection(collectionName);
            collection.insert(struct, options, function(err, result) {
              if (err) {
                return reject(err);
              } else {
                return resolve(result.ops);
              }
            });
          }, function(err) {
            return reject(err);
          });
      } catch (err) {
        console.log('insert catch', err);
        reject(err);
      }
    });
    return promise;
  };

/**
 * Encuentra un registro unico del modelo
 * - collectionName <String> Nombre del collection
 * - query <Object> criterio de busqueda
 * - fields <Object> opciones de campos de vuelta
 * returns: <Promise>
 */
module.exports.pFindOneHelper =
  function pFindOneHelper(collectionName, query, fields) {
    var promise;
    //
    query = typeof query === 'undefined' ? {} : query;
    //
    if (query._id) {
      query = idParser(query);
    }
    //
    promise = when.promise(function(resolve, reject) {
      try {
        _pOpenConn()
          .then(function(db) {
              var collection = db.collection(collectionName);
              collection.findOne(
                query,
                fields || {},
                function(err, doc) {
                  if (err) {
                    return reject(err);
                  } else {
                    return resolve(doc);
                  }
                });
            },
            function(err) {
              return reject(err);
            });
      } catch (err) {
        return reject(err);
      }
    });
    return promise;
  };

/**
 * Encuentra multiples registros del modelo
 * - collectionName <String> Nombre del collection
 * - query <Object> criterio de busqueda
 * - fields <Object> opciones de campos de vuelta
 * - opciones <Object> opciones de resultados
 * returns: <Promise>
 */
module.exports.pFindManyHelper =
  function pFindManyHelper(collectionName, query, fields, options) {
    var promise;
    //
    query = typeof query === 'undefined' ? {} : query;
    //
    if (query._id) {
      query = idParser(query);
    }
    //
    promise = when.promise(function(resolve, reject) {
      try {
        _pOpenConn()
          .then(function(db) {
              var collection = db.collection(collectionName);
              collection.find(
                query,
                fields || {},
                options || {}).toArray(
                function(err, doc) {
                  if (err) {
                    return reject(err);
                  } else {
                    return resolve(doc);
                  }
                });
            },
            function(err) {
              return reject(err);
            });

      } catch (err) {
        return reject(err);
      }
    });
    return promise;
  };