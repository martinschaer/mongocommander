'use strict';

var Commander = require('../index'),
  Helpers = {
    dropCollections: function(done) {
      Commander.pOpenConn()
        .then(function(db) {
          db.dropDatabase(function(err) {
            if (err) {
              done(err);
            } else {
              console.log('DataBase Dropped');
              done();
            }
          });
        }, function(err) {
          done(err);
        });
    }
  };

module.exports = Helpers;