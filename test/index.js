/* global describe, it, before */
'use strict';

var chai = require('chai'),
  chaiAsPromised = require('chai-as-promised'),
  Commander = require('../index'),
  helpers = require('./helpers'),
  url = 'mongodb://localhost/mongocommandertest';

chai.use(chaiAsPromised);
chai.should();

describe('#create', function() {
  before(function(done) {
    Commander
      .pSetUpUrl(url)
      .then(function(message) {
        console.log(message);
        helpers.dropCollections(function(err) {
          if (err) done(err);
          done();
        });
      }, function(err) {
        done(err);
      });
  });
  it('creates a new User document', function(done) {
    Commander.pCreateHelper('users', [{
      email: 'martin@interaction.cr'
    }]).should.eventually.be.a('array').with.length(1).notify(done);
  });
});

describe('#find', function() {
  it('finds a User document', function(done) {
    Commander.pFindOneHelper('users', {
      email: 'martin@interaction.cr'
    }).should.eventually.have.property('email', 'martin@interaction.cr')
      .notify(done);
  });

  it('finds all User documents', function(done) {
    Commander.pFindManyHelper('users', {})
      .should.eventually.be.a('array').with.length(1)
      .notify(done);
  });
});

describe('#updates', function() {
  it('find and modify a User document', function(done) {
    Commander.pFindAndModHelper('users', {
      email: 'martin@interaction.cr'
    }, {}, {
      $set: {
        email: 'j@interaction.cr'
      }
    }).should.eventually.have.property('email', 'martin@interaction.cr')
      .notify(done);
  });

  it('find and modify a User document and return new', function(done) {
    Commander.pFindAndModHelper('users', {
      email: 'j@interaction.cr'
    }, {}, {
      $set: {
        email: 'martin@interaction.cr'
      }
    }, {
      new: true
    }).should.eventually.have.property('email', 'martin@interaction.cr')
      .notify(done);
  });

  it('update a User document', function(done) {
    Commander.pUpdateHelper('users', {
      email: 'martin@interaction.cr'
    }, {}, {
      $set: {
        email: 'j@interaction.cr'
      }
    }).should.eventually.be.have.property('n', 1)
      .notify(done);
  });
});